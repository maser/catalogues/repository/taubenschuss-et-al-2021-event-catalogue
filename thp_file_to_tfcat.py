#!python3

"""
MIT License

Copyright (c) 2021 Baptiste Cecconi (Observatoire de Paris, PADC/MASER)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

__author__ = 'Baptiste Cecconi'
__copyright__ = 'Copyright 2021, Baptiste Cecconi, PADC/MASER'
__credits__ = ['Baptiste Cecconi']
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = 'Baptiste Cecconi'
__email__ = 'baptiste.cecconi@obspm.fr'
__status__ = "development"

from tfcat.feature import Feature, FeatureCollection, CRS
from tfcat.geometry import MultiPoint
from tfcat.codec import dump
from tfcat.validate import validate_file
from scipy.io import readsav
from pathlib import Path
from astropy.time import Time
from astropy.units import Unit as u
import sys
import os
import shlex, subprocess
from jsonschema.exceptions import ValidationError

SCHEMA_URI = "https://voparis-ns.obspm.fr/maser/tfcat/v1.0/tfcat-v1.0.json"
THP_DATA_URL = "https://maser.obspm.fr/doi/10.25935/r11g-6j63/content/cas_scan_all.thp"
DATA_DIR = Path(__file__).parent / "data"
THP_DATA_FILE = DATA_DIR / "cas_scan_all.thp"
SAV_DATA_FILE = DATA_DIR / "cas_scan_all.sav"


def exec_command(command_line, verbose=False):
    """Execute a shell command

    :param command_line: shell command string
    :param verbose: verbose flag
    """
    if verbose:
        print(f'Running: {command_line}\n')
    args = shlex.split(command_line)
    p = subprocess.run(args, capture_output=True)
    if verbose:
        print(p)


def download_thp_file(file_url=THP_DATA_URL, file_path=THP_DATA_FILE, verbose=False):
    """Download the .THP file from the web. Uses "curl".

    :param file_url: URL of THP data file path
    :param file_path: local path of file
    :param verbose: verbose flag
    """
    cmd = f"curl -o data/{file_path} {file_url}"
    exec_command(cmd, verbose=verbose)


def thp_to_sav(
        input_file=THP_DATA_FILE,
        sav_file=SAV_DATA_FILE,
        verbose=False
    ):
    """Converts a .THP file into a .SAV file. Uses IDL.

    :param input_file: Input file path
    :type input_file: str or Path
    :param sav_file: Output file path
    :type sav_file: str or Path
    """

    # This works in bash, with the IDL configured (modified next line if needed)
    IDL_BIN_DIR = "/Applications/harris/idl87/bin"

    tmp_file_sav = sav_file

    # Run `thp_file_read.pro`
    command_line = f"""{IDL_BIN_DIR}/idl -e "thp_file_read, '{str(input_file)}', dat & save,file='{str(tmp_file_sav)}', dat & exit" """

    if verbose:
        print(command_line)
    exec_command(command_line, verbose=verbose)


def converter(input_file, verbose=False):
    """
    Converts a .THP or .SAV file into a TFCat JSON file

    :param input_file: Input file path
    :type input_file: str or Path
    """

    # if input_file is a .THP file, then convert it into .SAV
    if Path(input_file).name.endswith('.thp'):
        file_sav = SAV_DATA_FILE
        thp_to_sav(input_file, file_sav, verbose=verbose)
        tmp_file_sav = True
    else:
        file_sav = input_file
        tmp_file_sav = False

    # loading data from intermediate .sav file
    print('loading data...')

    raw_data = readsav(file_sav)

    # extracting data:
    tmin = Time(raw_data['dat']['TMIN'][0], format='unix')
    tmax = Time(raw_data['dat']['TMAX'][0], format='unix')
    fmin = raw_data['dat']['FMIN'][0] * u('Hz')
    fmax = raw_data['dat']['FMAX'][0] * u('Hz')
    nop = raw_data['dat']['NOP'][0]
    area = raw_data['dat']['AREA'][0]
    quality = raw_data['dat']['QUALITY'][0]

    id_border = raw_data['dat']['I01_BORDER'][0]
    tf_border = raw_data['dat']['TF_BORDER'][0]

    # setting up Feature list (of MultiPoint type items):
    print('building list of Features...')

    events = []
    for i in range(len(tmin)):
        events.append(Feature(
            id=i,
            geometry=MultiPoint(
                [(float(tt), float(ff)) for tt, ff in tf_border[id_border[i][0]:id_border[i][1]]]
            ),
            properties={
                'pix_cnt': int(nop[i]),
                'tf_area': float(area[i]),
                'quality': int(quality[i])
            },
            bbox=[float(tmin[i].unix), float(fmin[i].value), float(tmax[i].unix), float(fmax[i].value)]
        ))

    properties = {
        "instrument_host_name": raw_data['dat']['PROBE'][0].decode('ascii'),
        "instrument_name": "RPWS",
        "receiver_name": "HFR",
        "target_name": "Saturn",
        "target_class": "planet",
        "target_region": "magnetosphere",
        "feature_name": "SKR#Saturn Kilometric Radiation#Radio emissions",
        "title": "Catalogue of Faraday Rotation (FR) patches identified in Saturn Kilometric "
                 "Radiation (SKR) observations by Cassini/RPWS/HFR",
        "authors": [{
            "GivenName": "Ulrich",
            "FamilyName": "Taubenschuss",
            "ORCID": "https://orcid.org/0000-0003-4810-6958",
            "Affiliation": "https://ror.org/04vtzcr32"
        }, {
            "GivenName": "Baptiste",
            "FamilyName": "Cecconi",
            "ORCID": "https://orcid.org/0000-0001-7915-5571",
            "Affiliation": "https://ror.org/02eptjh02"
        }, {
            "GivenName": "Laurent",
            "FamilyName": "Lamy",
            "ORCID": "https://orcid.org/0000-0002-8428-1369",
            "Affiliation": "https://ror.org/02eptjh02"
        }],
        "bib_reference": "10.1016/j.icarus.2021.114661",
        "doi": "10.25935/R11G-6J63",
        "data_source_reference": "https://doi.org/10.25935/f8ns-0911",
        "publisher": "PADC",
        "version": "1.0"
    }

    fields = {
        "quality": {
            "info": "Quality parameter (this is biased strongly by my personal judgement, from visual inspection "
                    "of how clear the FR-fringes are visible in the Re{VuVw*} and Re{VvVw*}",
            "values": {
                "-1": "not set",
                "1": "good",
                "2": "medium",
                "3": "bad quality"
            },
            "datatype": "int",
            "ucd": "meta.code.qual"
        },
        "tf_area": {
            "info": "Area in the time-spectral plane",
            "unit": "s.Hz",
            "datatype": "float",
            "ucd": "em.freq;em.radio"
        },
        "pix_cnt": {
            "info": "number of spectral points occupied by an FR-patch (i.e., not just from the borderline, but "
                    "all points surrounded by that line)",
            "datatype": "int",
            "ucd": "meta.number"
        },
    }

    # This is the CRS definition: [unix timestamp, Hz] at Cassini
    crs = CRS({
        "type": "local",
        "properties": {
            "name": "Time-Frequency",
            "time_coords_id": "unix",
            "spectral_coords": {
                "type": "frequency",
                "unit": "Hz"
            },
            "ref_position_id": "cassini-orbiter"
        }
    })

    # writing to file
    print('writing to files...')

    # Split into yearly catalogues:
    yearly_catalogues = dict([(str(year), []) for year in range(2006, 2018)])
    for feature in events:
        tmin_year = crs.time_converter(feature.tmin).isot[0:4]
        yearly_catalogues[tmin_year].append(feature)

    maser_doi = properties['doi'].lower()
    maser_content_url = f'https://maser.obspm.fr/doi/{maser_doi}/content/'

    print('\nSPIP Table:\n-----------\n')
    print(f'|{{{{Year}}}}|{{{{# of Events}}}}|{{{{Size (MB)}}}}|{{{{File}}}}|{{{{Start Time}}}}|{{{{End Time}}}}|')
    for year, year_catalogue in yearly_catalogues.items():
        if len(year_catalogue) != 0:
            year_collection = FeatureCollection(
                schema=SCHEMA_URI,
                features=year_catalogue,
                properties=properties,
                fields=fields,
                crs=crs
            )
            year_collection.properties['time_min'] = crs.time_converter(year_catalogue[0].tmin).isot
            year_collection.properties['time_max'] = crs.time_converter(year_catalogue[-1].tmax).isot

            year_file = f'cassini_faraday_patches_{year}.json'
            with open(f'build/{year_file}', 'w') as f:
                dump(year_collection, f)

            try:
                validate_file(f'build/{year_file}', SCHEMA_URI)
            except ValidationError as e:
                raise e

            file_size = os.path.getsize(f'build/{year_file}')/(1024*1024)
            print(f'|{year}|{len(year_catalogue)}|{file_size:.2f}|[{year_file}->{maser_content_url}{year_file}]|' +
                  f'{year_collection.properties["time_min"]}|{year_collection.properties["time_max"]}|')
        else:
            print(f'|{year}|{len(year_catalogue)}||||')

    if tmp_file_sav:
        # cleaning temporay files
        print('clean up...')
        Path(file_sav).unlink()


# Upload files:
    print("\nscp commands:\n-------------\n")
    print("scp build/cassini_faraday_patches_20??.json vmmaser_rpws:/volumes/kronos/doi/10.25935/r11g-6j63/content")


if __name__ == "__main__":

    args = sys.argv[1:]

    verbose = False
    if '-v' in args:
        verbose = True
        args.remove('-v')

    if len(args) == 0:
        print('Using default filenames')
        if SAV_DATA_FILE.exists():
            input_file = SAV_DATA_FILE
        elif THP_DATA_FILE.exists():
            input_file = THP_DATA_FILE
        else:
            download_thp_file()
            input_file = THP_DATA_FILE
    else:
        input_file = Path(args[0])

    if not input_file.exists():
        raise FileNotFoundError(f"Input file doesn't exist: '{input_file}'")

    if len(args) > 1:
        raise IndexError('Too many arguments')

    if verbose:
        print(f'Input file: {input_file}')

    converter(input_file, verbose=verbose)
