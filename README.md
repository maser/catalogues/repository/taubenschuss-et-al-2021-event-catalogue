# Taubenschuss et al 2021 - Event catalogue

This script has been used to produce the supplementary material TFCat version 
of Taubenschuss et al. (2021).

The TFCat file is prepared from a THP file (specific to the Themis spacecraft 
team) containing a Faraday rotation patches identified in the Saturn kilometric
radio observation by Cassini/RPWS/HFR. 

## Run the example

NB: This example requires IDL

```bash
python thp_file_to_tfcat.py
```

## References

- Taubenschuss, U., L. Lamy, G. Fischer, D. Píša, O. Santolík, J. Souček, W. S. 
  Kurth, B. Cecconi, P. Zarka, & H. O. Rucker (**2021**). *The Faraday rotation 
  effect in Saturn Kilometric Radiation observed by the CASSINI spacecraft*. Icar, 
  370 114661. [doi:10.1016/j.icarus.2021.114661](https://doi.org/10.1016/j.icarus.2021.114661)
 